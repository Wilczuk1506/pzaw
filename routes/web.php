<?php

    use Illuminate\Support\Facades\Route;

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */

    Route::get('/', function () {
        echo "Hello world!";
    });

    // Route::get('greetings/{name}', function ($name) {
    //     echo "Witaj użytkowniku $name.";
    // });

    // Route::get('greetings/{name?}', function ($name = null) {
    //     if (empty($name)) {
    //         echo "Witaj! Jak masz na imię?";
    //     } else {
    //         echo "Cześć $name! Miło mi Ciebie poznać :)";
    //     }
    // });

    Route::get('greetings/{name?}', function ($name = null) {
        if (empty($name)) {
            echo "Witaj! Jak masz na imię?";
        } else {
            echo "Cześć $name! Miło mi Ciebie poznać :)";
        }
    })->whereAlpha('name');

    // Route::get('admin-panel/{code}', function ($code) {
    //     if ($code != 'test123') {
    //         abort(401, 'Podano nieprawidłowy kod dostępu.');
    //     }
    //     echo "Dostęp przyznany.";
    // })->whereAlphaNumeric('code');

    Route::get('admin-panel/{code}', function ($code) {
        abort_if($code != 'test123', 401, 'Podano nieprawidłowy kod dostępu.');
       
        echo "Dostęp przyznany";
    })->whereAlphaNumeric('code');

    Route::group(['prefix' => 'calculator'], function () {
        Route::get('addition/{num1}/{num2}', function ($num1, $num2) {
            echo $num1 + $num2;
        })->whereNumber('num1', 'num2');
        
        Route::get('subtraction/{num1}/{num2}', function ($num1, $num2) {
            echo $num1 - $num2;
        })->whereNumber('num1', 'num2');

        Route::get('multiplication/{num1}/{num2}', function ($num1, $num2) {
            echo $num1 * $num2;
        })->whereNumber('num1', 'num2');

        Route::get('division/{num1}/{num2}', function ($num1, $num2) {
            echo $num1 / $num2;
        })->whereNumber('num1', 'num2');

        Route::get('modulo/{num1}/{num2}', function ($num1, $num2) {
            echo $num1 % $num2;
        })->whereNumber('num1', 'num2');

        Route::get('power/{num1}/{num2}', function ($num1, $num2) {
            echo $num1 ** $num2;
        })->whereNumber('num1', 'num2');
    });

    Route::get('reverse/{string}', function ($string) {
        $array = ["-", "+", "_"];
        echo strrev(str_replace($array, " ", $string));
    });

    
    Route::group(['prefix' => 'quotes'], function (){
        $quotes = [
            0 => [
                "quote" => "Don’ get in my way! / Welcome to my world!",
                "hero" => "Viper",
            ],
            1 => [
                "quote" => "Watch this! / Get out of my way!",
                "hero" => "Jett",
            ],
            2 => [
                "quote" => "Let's go! / Off your feet!",
                "hero" => "Breach",
            ],
            3 => [
                "quote" => "Here comes the party! / Fire in the hole!",
                "hero" => "Raze",
            ],
            4 => [
                "quote" => "They will cower! / The hunt begins!",
                "hero" => "Reyna",
            ],
            5 => [
                "quote" => "Come on, let’s go! / Jokes over, you’re dead!",
                "hero" => "Phoenix",
            ],
            6 => [
                "quote" => "Where is everyone hiding? / I know EXACTLY where you are!",
                "hero" => "Cypher",
            ],
            7 => [
                "quote" => "Watch them run! / Scatter!",
                "hero" => "Omen",
            ],
            8 => [
                "quote" => "I am the hunter! / Nowhere to run!",
                "hero" => "Sova",
            ],
            9 => [
                "quote" => "Open up the sky! / Prepare for hellfire!",
                "hero" => "Brimstone",
            ],
            10 => [
                "quote" => "Your duty is not over! / You will not kill my allies!",
                "hero" => "Sage",
            ],
            11 => [
                "quote" => "Initiated! / You should run!",
                "hero" => "Killjoy",
            ],
            12 => [
                "quote" => "Seek them out! / I've got your trail!",
                "hero" => "Skye",
            ],
            13 => [
                "quote" => "I’ll handle this! / You're next!",
                "hero" => "Yoru",
            ],
            14 => [
                "quote" => "World divided! / You’re divided!",
                "hero" => "Astra",
            ],
            15 => [
                "quote" => "No one walks away! / You. Are. Powerless!",
                "hero" => "KAY/O",
            ],
            16 => [
                "quote" => "They are so dead! / You want to play? Let's play!",
                "hero" => "Chamber",
            ],
            17 => [
                "quote" => "Here we go! / Oy! I’m pissed!",
                "hero" => "Neon",
            ],
            18 => [
                "quote" => "Face your fear! / Nightmare take them!",
                "hero" => "Fade",
            ],
            19 => [
                "quote" => "Let’s turn the tide! / I suggest you move!",
                "hero" => "Harbor",
            ],
            20 => [
                "quote" => "It’s all you, lil’ homie! / Oye! Monster on the loose!",
                "hero" => "Gekko",
            ],
            21 => [
                "quote" => "Pull them to their grave! / My territory, my rules!",
                "hero" => "Deadlock",
            ],
        ];
        Route::get('{id?}', function ($id = null) use ($quotes){
            if(empty($id)){
                //losuj kwestię i ją wyświetl
                $temp_id = rand(0, 21);
                echo $quotes[$temp_id]['quote'];
            }
            else if(!array_key_exists($id, $quotes)){
                abort(404, "Nie znaleziono bohatera");
            } 
            else {
                echo $quotes[$id]['quote'];
            }
        })->whereNumber('id');
    
        Route::get('{id}/hero', function ($id) use ($quotes){
            echo $quotes[$id]['hero'];
        })->whereNumber('id');

        Route::get('{id}/guess/{hero}', function ($id, $hero) use ($quotes){
            if(strcasecmp($hero, $quotes[$id]['hero']) == 0){
                echo "Poprawnie zgadnięto";
            } else {
                echo "U Stoopyd";
            }
        })->whereNumber('id');
    });
    use App\Models\Publication;
    use App\Models\Character;
    use App\Models\User;
    use App\Http\Controllers\PublicationController;
    use App\Http\Controllers\SiteController;
    use Carbon\Carbon;
    use App\Http\Controllers\UserController;
    Route::group(['prefix' => 'glowna'], function() {
        Route::get('home/', [SiteController::class, 'home'])->name('home');

        Route::get('about_us/', [SiteController::class, 'about_us'])->name('about_us');

        Route::get('publications/', [PublicationController::class, 'index'])->name('PublicationsAll');
        
        Route::get('publications/{id}', [PublicationController::class, 'show'])->WhereNumber('id')->name('PublicationsId');

        Route::get('quotes/list/', [SiteController::class, 'quotes'])->name('quotes');

        Route::get('user/{id}', [SiteController::class, 'user'])->name('UserId');

        Route::get('publications/create/', [PublicationController::class, 'create'])->name('createForm');

        Route::post('publications/store/', [PublicationController::class, 'store'])->name('storeFunction');

        Route::get('post/{publication}/edit', [PublicationController::class, 'edit'])->name('PublicationEdit');

        Route::put('post/{publication}', [PublicationController::class, 'update'])->name('PublicationUpdate');

        Route::delete('publications/{publication}', [PublicationController::class, 'destroy'])->name('PublicationDelete');

        Route::get('users/register', [UserController::class, 'create'])->name('UsersCreate');

        Route::post('users/store', [UserController::class, 'store'])->name('UsersStore');
    });
?>