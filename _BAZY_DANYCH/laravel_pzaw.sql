-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 15 Lis 2023, 18:28
-- Wersja serwera: 10.4.24-MariaDB
-- Wersja PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `laravel_pzaw`
--
CREATE DATABASE IF NOT EXISTS `laravel_pzaw` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_polish_ci;
USE `laravel_pzaw`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `characters`
--

DROP TABLE IF EXISTS `characters`;
CREATE TABLE `characters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `quote` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `hero` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `characters`
--

INSERT INTO `characters` (`id`, `quote`, `hero`, `role`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Don`t get in my way! / Welcome to my world!', 'Viper', 'Controller', 'https://cdn.discordapp.com/attachments/900838594212659211/1161332205914693784/image.png?ex=6537e9e6&is=652574e6&hm=161727b986b59be8ef9758ac9b7ee4f75ea16d880d665b3df7aee729104ad68e&', '2023-11-13 15:49:49', '2023-11-13 15:49:49'),
(2, 'Watch this! / Get out of my way!', 'Jett', 'Duelist', 'https://cdn.discordapp.com/attachments/900838594212659211/1161332316392673320/250.png?ex=6537ea00&is=65257500&hm=16dde331cc0fe2a0b1c88148bfbbe603a5a1f0974a59ef8c47ea7dac63a87e22&', '2023-11-13 15:49:49', '2023-11-13 15:49:49'),
(3, 'Let`s go! / Off your feet!', 'Breach', 'Initiator', 'https://cdn.discordapp.com/attachments/900838594212659211/1161332453219238040/250.png?ex=6537ea21&is=65257521&hm=7f9932a08c2f0b6cc8161231434b092a23667ebd5bf88c37c825d882bca24f1b&', '2023-11-13 15:49:49', '2023-11-13 15:49:49'),
(4, 'Here comes the party! / Fire in the hole!', 'Raze', 'Duelist', 'https://cdn.discordapp.com/attachments/900838594212659211/1161332568176734288/250.png?ex=6537ea3c&is=6525753c&hm=17f75aed410f0908879aef287a3f19f10f1ca7d8786e68ea2dd18da150d8ed6d&', '2023-11-13 15:49:49', '2023-11-13 15:49:49'),
(5, 'They will cower! / The hunt begins!', 'Reyna', 'Duelist', 'https://cdn.discordapp.com/attachments/900838594212659211/1161332642503983314/250.png?ex=6537ea4e&is=6525754e&hm=73f99abb049fadc0bbb1bf14662df7e675e8d33226ced8860a5135723d916504&', '2023-11-13 15:49:49', '2023-11-13 15:49:49'),
(6, 'Come on, let`s go! / Jokes over, you`re dead!', 'Phoenix', 'Duelist', 'https://cdn.discordapp.com/attachments/900838594212659211/1161332728776622100/250.png?ex=6537ea63&is=65257563&hm=c34f1cb52583c05fa85e492006db5691e35aa59e187129a5e66016dd6dc340eb&', '2023-11-13 15:49:49', '2023-11-13 15:49:49'),
(7, 'Where is everyone hiding? / I know EXACTLY where you are!', 'Cypher', 'Sentinel', 'https://cdn.discordapp.com/attachments/900838594212659211/1161332815732949133/250.png?ex=6537ea77&is=65257577&hm=822b40b33be06c8dccf5587b2a1f3d28128893ee1a985f9576db1302bac34387&', '2023-11-13 15:49:49', '2023-11-13 15:49:49'),
(8, 'Watch them run! / Scatter!', 'Omen', 'Controller', 'https://cdn.discordapp.com/attachments/900838594212659211/1161334051446198342/250.png?ex=6537eb9e&is=6525769e&hm=a7e8996ce407dd5e8e18f253872e136132889bb4444fc07c929210d0920f111f&', '2023-11-13 15:49:49', '2023-11-13 15:49:49'),
(9, 'I am the hunter! / Nowhere to run!', 'Sova', 'Initiator', 'https://cdn.discordapp.com/attachments/900838594212659211/1161334141472743464/250.png?ex=6537ebb3&is=652576b3&hm=ee3310f24f05e543194ec3b00f9ca7992257c8882ee222b077e4927da6800558&', '2023-11-13 15:49:49', '2023-11-13 15:49:49'),
(10, 'Open up the sky! / Prepare for hellfire!', 'Brimstone', 'Controller', 'https://cdn.discordapp.com/attachments/900838594212659211/1161334251527077928/250.png?ex=6537ebce&is=652576ce&hm=67fd09bcf4bcde4bc7ac12c0384521de98393db4fb8240ffc65fcc26cf3336e6&', '2023-11-13 15:49:49', '2023-11-13 15:49:49'),
(11, 'Your duty is not over! / You will not kill my allies!', 'Sage', 'Sentinel', 'https://cdn.discordapp.com/attachments/900838594212659211/1161334331659276429/250.png?ex=6537ebe1&is=652576e1&hm=71f83069273d8b769b32501bd484aa19d5fa8499f9b7682b9ceee59717314b39&', '2023-11-13 15:49:49', '2023-11-13 15:49:49'),
(12, 'Initiated! / You should run!', 'Killjoy', 'Sentinel', 'https://cdn.discordapp.com/attachments/900838594212659211/1161335324920774727/250.png?ex=6537ecce&is=652577ce&hm=377ea45e28da87ae4fa9ac393ad3de93b1907b603bd1dacac0a381fbc66859ac&', '2023-11-13 15:49:49', '2023-11-13 15:49:49'),
(13, 'Seek them out! / I`ve got your trail!', 'Skye', 'Initiator', 'https://cdn.discordapp.com/attachments/900838594212659211/1161335388535795762/250.png?ex=6537ecdd&is=652577dd&hm=9c48f9e585f97f8064edfd8da5b04672eb1be7b263fb3ed47a29755a7e65c0a8&', '2023-11-13 15:49:49', '2023-11-13 15:49:49'),
(14, 'I`ll handle this! / You`re next!', 'Yoru', 'Duelist', 'https://cdn.discordapp.com/attachments/900838594212659211/1161335466860232845/250.png?ex=6537ecef&is=652577ef&hm=e3ea42983da6e37d20f0f9e16e6b1762db0b94d213940dc4a01b6acbcdc9ab76&', '2023-11-13 15:49:49', '2023-11-13 15:49:49'),
(15, 'World divided! / You`re divided!', 'Astra', 'Controller', 'https://cdn.discordapp.com/attachments/900838594212659211/1161335547587993620/250.png?ex=6537ed03&is=65257803&hm=c3053599b0c3da6ac0094b570215b29fbc25fee50273f48c0368788f44fbda6d&', '2023-11-13 15:49:49', '2023-11-13 15:49:49'),
(16, 'No one walks away! / You. Are. Powerless!', 'KAY/O', 'Initiator', 'https://cdn.discordapp.com/attachments/900838594212659211/1161335648196772001/250.png?ex=6537ed1b&is=6525781b&hm=2589d31512a31bba0e3dfc6d24bc190d230de24d49297b31dc64cb364d2373b5&', '2023-11-13 15:49:49', '2023-11-13 15:49:49'),
(17, 'They are so dead! / You want to play? Let`s play!', 'Chamber', 'Sentinel', 'https://cdn.discordapp.com/attachments/900838594212659211/1161335712038273164/250.png?ex=6537ed2a&is=6525782a&hm=8ed37f93268095a8462ea0254fa399ffce42169f7023a8baf8eda0543358c963&', '2023-11-13 15:49:49', '2023-11-13 15:49:49'),
(18, 'Here we go! / Oy! I`m pissed!', 'Neon', 'Duelist', 'https://cdn.discordapp.com/attachments/900838594212659211/1161335776697667654/250.png?ex=6537ed39&is=65257839&hm=63941d9d1d9243f4e26a63edb2bc81b90ae9be6cba6ec233b308bbc838f53988&', '2023-11-13 15:49:49', '2023-11-13 15:49:49'),
(19, 'Face your fear! / Nightmare take them!', 'Fade', 'Initiator', 'https://cdn.discordapp.com/attachments/900838594212659211/1161335876899573803/250.png?ex=6537ed51&is=65257851&hm=58fc9c69092a29e7fef71ae58131ec05f7b826bb3f62c34463b8f2bfc9d64a56&', '2023-11-13 15:49:49', '2023-11-13 15:49:49'),
(20, 'Let`s turn the tide! / I suggest you move!', 'Harbor', 'Controller', 'https://cdn.discordapp.com/attachments/900838594212659211/1161335954926227566/250.png?ex=6537ed64&is=65257864&hm=6460f2dcb2735d4dd9b95e77fc70d63a71a98a8506133dcbe854c5704f9c4bac&', '2023-11-13 15:49:49', '2023-11-13 15:49:49'),
(21, 'It`s all you, lil` homie! / Oye! Monster on the loose!', 'Gekko', 'Initiator', 'https://cdn.discordapp.com/attachments/900838594212659211/1161336035033231430/250.png?ex=6537ed77&is=65257877&hm=40c6c8c43f88e1e702feae93874405fb0a09b6c3a08e516d1e84bfc6387403cc&', '2023-11-13 15:49:49', '2023-11-13 15:49:49'),
(22, 'Pull them to their grave! / My territory, my rules!', 'Deadlock', 'Sentinel', 'https://cdn.discordapp.com/attachments/900838594212659211/1161336113001152522/250.png?ex=6537ed89&is=65257889&hm=bf1b80e0a7e2e5999274450a5ea6425477e734e60a27727af9b3cb39a944d838&', '2023-11-13 15:49:49', '2023-11-13 15:49:49');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `author_id` bigint(20) UNSIGNED NOT NULL,
  `publication_id` bigint(20) UNSIGNED NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `comments`
--

INSERT INTO `comments` (`id`, `author_id`, `publication_id`, `content`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'w 100% się z tobą zgadzam!', '2023-11-13 15:49:49', '2023-11-13 15:49:49'),
(2, 1, 1, 'a ja nie L!', '2023-11-13 15:49:49', '2023-11-13 15:49:49'),
(3, 1, 2, 'Dobrze zrobiła, ale wy problemy macie', '2023-11-13 15:49:49', '2023-11-13 15:49:49'),
(4, 1, 3, 'JAKI DEBIL AAAAAAAAAAAAAAAAAAAHAHAHAHAHHAHA', '2023-11-13 15:49:49', '2023-11-13 15:49:49');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_10_23_115011_create_publications_table', 1),
(6, '2023_10_25_213146_create_characters_table', 1),
(7, '2023_11_12_153905_alter_publications_table_change_author_column', 1),
(8, '2023_11_13_161940_create_comments_table', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `password_reset_tokens`
--

DROP TABLE IF EXISTS `password_reset_tokens`;
CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `publications`
--

DROP TABLE IF EXISTS `publications`;
CREATE TABLE `publications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `author_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `publications`
--

INSERT INTO `publications` (`id`, `title`, `content`, `author_id`, `created_at`, `updated_at`) VALUES
(1, 'Iso - nowa postać w Valorant ssie', 'Iso, najnowsza postać, która ma zostać dodana do słynnej gry Valorant, okazał się bardzo słabym i overhypowanym agentem.', 1, '2023-11-13 15:49:49', '2023-11-13 15:49:49'),
(2, 'Mommy Sage nie leczy teammatów', 'W ostatniej grze Oskara kiedy poprosił a heala od (cytując) \"Mommy Sage\", ta odmóiła, więc pan gamer przejechał się do jej domu. Po więcej informacji proszę udać się do najbliższego posterunku policji.', 1, '2023-11-13 15:49:49', '2023-11-13 15:49:49'),
(3, 'No Aces?', 'Igor Nadkierniczny dalej nie jest w stanie zdobyć żadnego Acea w popularnej grze FPS Valorant XDDDDDDDDDDDDDDDDDD, JAAKIE SKILL ISSUE WGL, LOOOOOOOOOOOOOOOOOOOOL, L BOZO', 1, '2023-12-31 23:00:00', '2023-11-13 15:49:49');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Test User', 'test@example.com', '2023-11-13 15:49:48', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'rPVdXzIiOw', '2023-11-13 15:49:49', '2023-11-13 15:49:49');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `characters`
--
ALTER TABLE `characters`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_author_id_foreign` (`author_id`);

--
-- Indeksy dla tabeli `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indeksy dla tabeli `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indeksy dla tabeli `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indeksy dla tabeli `publications`
--
ALTER TABLE `publications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `publications_author_id_foreign` (`author_id`);

--
-- Indeksy dla tabeli `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT dla zrzuconych tabel
--

--
-- AUTO_INCREMENT dla tabeli `characters`
--
ALTER TABLE `characters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT dla tabeli `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT dla tabeli `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `publications`
--
ALTER TABLE `publications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`);

--
-- Ograniczenia dla tabeli `publications`
--
ALTER TABLE `publications`
  ADD CONSTRAINT `publications_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
