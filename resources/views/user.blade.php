@extends('layouts.main')
@section('content')
        <div class="box">
            <h2>Nazwa</h2>
            <p>{{$user->name}}</p>
            <h2>Email</h2>
            <p>{{$user->email}}</p>
            <h2>Data utworzenia użytkownika</h2>
            <p>{{$user->created_at}}</p>
            <h2>Opublikowane wpisy</h2>
            @foreach($user->publications as $pub)
                <p><a href="{{ route('PublicationsId',['id' => $pub->id]) }}">{{$pub->title}}</a></p>
            @endforeach
        </div>

@endsection