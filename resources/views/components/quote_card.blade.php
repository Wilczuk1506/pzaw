@props(['hero', 'quote', 'role', 'image'])
<figure>
    <img src="{{ $image }}" alt="author" class="qoute-card-img">
    <div class="">
        <figcaption>
            <p class="first-p">Imię osoby: {{ $hero }}</p>
            <p>Kwestia osoby: {{ $quote }}</p>
            <p>Posada osoby: {{ $role }}</p>
        </figcaption>
    </div>
</figure>