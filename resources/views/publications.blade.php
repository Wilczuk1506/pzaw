@extends('layouts.main')

@section('content')
    <h1 class="heading">Publikacje YIPPEE!</h1>

    @foreach($quotes as $q)
        <x-card hero="{{$q['hero']}}" quote="{{$q['quote']}}"/>
    @endforeach
@endsection