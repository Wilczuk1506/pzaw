@extends('layouts.main')
@section('content')
    <h1 class="heading">Sample Text</h1>

    <div class="quotes-grid-div">
    @foreach($quotes as $q)
        <div class="quotes-character-chard">
            <x-quote_card hero="{{$q->hero}}" quote="{{$q->quote}}" role="{{$q->role}}" image="{{$q->image}}"/>
        </div>
    @endforeach
    </div>
@endsection