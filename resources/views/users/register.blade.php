@extends('layouts.main')

@section('content')
    <h1>Zarejestruj się</h1>
    <form action="{{route('UsersStore')}}" method="POST">
        @csrf
        <label for="name">Nazwa</label><br>
        <input type="text" name="name" placeholder="Nazwa" value="{{ old('name') }}">
        @error('name')
            <p>{{ $message }}</p>
        @enderror
        <br>
        <label for="email">Email</label><br>
        <input type="email" name="email" placeholder="Email" value="{{ old('email') }}">
        @error('email')
            <p>{{ $message }}</p>
        @enderror
        <br>
        <label for="password">Hasło</label><br>
        <input type="password" name="password" placeholder="Hasło">
        <label for="password_confirmation"></label><br>
        <input type="password" name="password_confirmation" placeholder="Ponów Hasło">
        @error('password')
            <p>{{ $message }}</p>
        @enderror
        <br>
        <button type="submit">Zapisz</button>
    </form>
@endsection