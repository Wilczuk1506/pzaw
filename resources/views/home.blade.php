@extends('layouts.main')

@section('content')
    <h1 class="heading">Witaj na naszej jak na razie pustej stronie!</h1>
    <p>Dzisiaj jest {{ $date }}</p>
    
    <h2>Tytuł</h2>
    <p>{{$publication->title}}</p>
    <h2>Treść</h2>
    <p>{{$publication->excerpt}}<a href="{{ route('PublicationsId',['id' => $publication->id]) }}" class="box-link">[Czytaj więcej]</a></p>
    <h2>Autor</h2>
    <p>{{$publication->author->name}}</p>
    <h2>Post Stworzony:</h2>
    <p>{{$data}}</p>
@endsection