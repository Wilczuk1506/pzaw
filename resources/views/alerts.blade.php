@if (session()->has('success'))
	<div class="alert success" role="alert">
    	<div>
        	{{ session('success') }}
    	</div>
	</div>
@endif

@if (session()->has('warning'))
	<div class="alert warning" role="alert">
    	<div>
        	{{ session('warning') }}
    	</div>
    </div>
@endif

@if (session()->has('error'))
	<div class="alert error" role="alert">
    	<div>
        	{{ session('error') }}
    	</div>
	</div>
@endif