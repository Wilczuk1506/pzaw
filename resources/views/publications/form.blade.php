@extends('layouts.main')

@php
    $action = route('storeFunction');
    $title = old('title');
    $content = old('content');
    $author_id = null;
    $header = "Tworzenie nowej publikacji";

    if (! empty($publication)) {
        $action = route('PublicationUpdate', ['publication' => $publication->id]);
        $title = $publication->title;
        $content = $publication->content;
        $author_id = $publication->author_id;
        $header = "Edycja publikacji";
    }
@endphp


@section('content')
    <h1>{{$header}}</h1>
    <form action="{{$action}}" method="POST">
        @if (! empty($publication))
            @method('PUT')
        @endif
        @csrf
        <label for="title">Tytuł</label><br>
        <input value="{{$title}}" type="text" name="title" id="" placeholder="Tytuł">
        @error('title')
            <p>{{ $message }}</p>
        @enderror
        <br>
        <label for="content">Treść</label><br>
        <textarea name="content" id="" cols="30" rows="10" placeholder="Treść...">{{$content}}</textarea>
        @error('content')
            <p>{{ $message }}</p>
        @enderror
        <br>
        <label for="author_id">Autor</label><br>
        <select name="author_id" id="">
            <option value="">--Wybierz autora--</option>
            @foreach($users as $user)
                <option value="{{ $user->id }}" @selected($author_id == $user->id)>{{ $user->name }}</option>
            @endforeach
        </select>
        <!-- <input type="number" name="author_id" id="" min="1" placeholder="ID autora"> -->
        @error('author_id')
            <p>{{ $message }}</p>
        @enderror
        <br>
        <button type="submit">Zapisz</button>
    </form>
@endsection