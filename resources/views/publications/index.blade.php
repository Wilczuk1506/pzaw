<?php
use Carbon\Carbon;
?>
@extends('layouts.main')

@section('content')
    <h1><a href="{{route('createForm')}}" class="box-link">Utwórz publikację</a></h1>
    @foreach($publications as $p)
        <div class="box">
            <h2>Tytuł</h2>
            <p>{{$p->title}}</p>
            <h2>Treść</h2>
            <p>{{$p->excerpt}} <a href="{{ route('PublicationsId',['id' => $p->id]) }}" class="box-link">[Czytaj więcej]</a></p>
            <h2>Autor</h2>
            @if (is_null($p->author->deleted_at))
                <p>{{$p->author->name}}</p>
            @else
                <p><s>{{$p->author->name}}</s></p>
            @endif
            <h2>Post Stworzony:</h2>
            <p>{{Carbon::create($p->created_at)->diffForHumans(Carbon::now())}}</p>
            <p><a href="{{route('PublicationEdit', ['publication' => $p->id])}}" class="box-link">[Edytuj]</a></p>
        </div>
    @endforeach
@endsection