@extends('layouts.main')

@section('content')
    <div class="box">
        <h2>Tytuł</h2>
        <p>{{$publication->title}}</p>
        <h2>Treść</h2>
        <p>{{$publication->content}}</p>
        <h2>Autor</h2>
        @if (is_null($publication->author->deleted_at))
            <p>{{$publication->author->name}}</p>
        @else
            <p><s>{{$publication->author->name}}</s></p>
        @endif
        <h2>Post Stworzony:</h2>
        <p>{{$data}}</p>
        <h2>Komentarze</h2>
        @foreach($comments as $com)
            <h3>Autor</h3>
            @if (is_null($publication->author->deleted_at))
                <p>{{$com->author->name}}</p>
            @else
                <p><s>{{$com->author->name}}</s></p>
            @endif
            <h3>Treść</h3>
            <p>{{$com->content}}</p>
        @endforeach
        <form action="{{ route('PublicationDelete', ['publication' => $publication->id]) }}" method="POST">
            @csrf
            @method('DELETE')
            <button type="submit">Usuń</button>
        </form>
    </div>
@endsection