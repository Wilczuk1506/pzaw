<!DOCTYPE html>
<html lang="pl">
    <head>
        <meta charset="UTF-8">
        <script src="https://unpkg.com/feather-icons"></script>
        <link rel="stylesheet" type="text/css" href="{{ asset('style.css') }}">
        <script src="{{ asset('script.js') }}"></script>
        
        <title>{{config('app.name')}}</title>
    </head>
    <body>
        <nav>
            <img class="nav-img" src="https://media.discordapp.net/attachments/899369022590709832/1161711911340085389/image.png?ex=65394b87&is=6526d687&hm=8392b32f224887e87d6b6aad82eb8e11985f42cdb41d79e4844ae5f45d48040a&=" alt="logo">
            <ul class="nav-list">
                <svg class="kreska">
                    <rect width="3" height="50" fill="rgb(15 23 42)" rx="500" ry="500"></rect>
                </svg>
                <a href="{{route('home')}}" class="nav-list-element-a hover-and-hold">
                    <li class="nav-list-element">
                        <i data-feather="home" class="nav-list-element-icon"></i>
                        Home
                    </li>
                </a>
                <svg class="kreska">
                    <rect width="3" height="50" fill="rgb(15 23 42)" rx="500" ry="500"></rect>
                </svg>
                <a href="{{route('about_us')}}" class="nav-list-element-a hover-and-hold">
                    <li class="nav-list-element">
                        <i data-feather="info" class="nav-list-element-icon"></i>
                        About us
                    </li>
                </a>
                <svg class="kreska">
                    <rect width="3" height="50" fill="rgb(15 23 42)" rx="500" ry="500"></rect>
                </svg>
                <a href="{{route('PublicationsAll')}}" class="nav-list-element-a hover-and-hold">
                    <li class="nav-list-element">
                        <i data-feather="archive" class="nav-list-element-icon"></i>
                        Publications
                    </li>
                </a>
                <svg class="kreska">
                    <rect width="3" height="50" fill="rgb(15 23 42)" rx="500" ry="500"></rect>
                </svg>
                <a href="{{route('quotes')}}" class="nav-list-element-a hover-and-hold">
                    <li class="nav-list-element">
                        <i data-feather="message-square" class="nav-list-element-icon"></i>
                        Quotes
                    </li>
                </a>
                <svg class="kreska">
                    <rect width="3" height="50" fill="rgb(15 23 42)" rx="500" ry="500"></rect>
                </svg>
            </ul>
            <a href="{{route('UsersCreate')}}">
                <button class="nav-button">
                    Rejestruj
                </button>
            </a>
        </nav>
        <main class="content-div">
            @include('alerts')
            
            @yield('content')
        </main>
        <footer>
            Copyright 2023 Wiktor Ilczuk
        </footer>
    </body>
    <script defer>feather.replace();</script>
</html>