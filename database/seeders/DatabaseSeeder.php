<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Publication;
use App\Models\Character;
use App\Models\User;
use App\Models\Comment;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        $testUser = User::factory()->create([
            'name' => 'Test User',
            'email' => 'test@example.com',
        ]);
        Publication::create([
            'title' => 'Iso - nowa postać w Valorant ssie',
            'content' => 'Iso, najnowsza postać, która ma zostać dodana do słynnej gry Valorant, okazał się bardzo słabym i overhypowanym agentem.',
            'author_id' => $testUser->id,
        ]);
        Publication::create([
            'title' => 'Mommy Sage nie leczy teammatów',
            'content' => 'W ostatniej grze Oskara kiedy poprosił a heala od (cytując) "Mommy Sage", ta odmóiła, więc pan gamer przejechał się do jej domu. Po więcej informacji proszę udać się do najbliższego posterunku policji.',
            'author_id' => $testUser->id,
        ]);
        Publication::create([
            'title' => 'No Aces?',
            'content' => 'Igor Nadkierniczny dalej nie jest w stanie zdobyć żadnego Acea w popularnej grze FPS Valorant XDDDDDDDDDDDDDDDDDD, JAAKIE SKILL ISSUE WGL, LOOOOOOOOOOOOOOOOOOOOL, L BOZO',
            'author_id' => $testUser->id,
            'created_at' => '2024-01-01',
        ]);
        Comment::create([
            'author_id' => $testUser->id,
            'publication_id' => 1,
            'content' => 'w 100% się z tobą zgadzam!'
        ]);
        Comment::create([
            'author_id' => $testUser->id,
            'publication_id' => 1,
            'content' => 'a ja nie L!'
        ]);
        Comment::create([
            'author_id' => $testUser->id,
            'publication_id' => 2,
            'content' => 'Dobrze zrobiła, ale wy problemy macie'
        ]);
        Comment::create([
            'author_id' => $testUser->id,
            'publication_id' => 3,
            'content' => 'JAKI DEBIL AAAAAAAAAAAAAAAAAAAHAHAHAHAHHAHA'
        ]);
        Character::create([
            "quote" => "Don`t get in my way! / Welcome to my world!",
            "hero" => "Viper",
            "role" => "Controller",
            "image" => "https://cdn.discordapp.com/attachments/900838594212659211/1161332205914693784/image.png?ex=6537e9e6&is=652574e6&hm=161727b986b59be8ef9758ac9b7ee4f75ea16d880d665b3df7aee729104ad68e&",
        ]);
        Character::create([
            "quote" => "Watch this! / Get out of my way!",
            "hero" => "Jett",
            "role" => "Duelist",
            "image" => "https://cdn.discordapp.com/attachments/900838594212659211/1161332316392673320/250.png?ex=6537ea00&is=65257500&hm=16dde331cc0fe2a0b1c88148bfbbe603a5a1f0974a59ef8c47ea7dac63a87e22&",
        ]);
        Character::create([
            "quote" => "Let`s go! / Off your feet!",
            "hero" => "Breach",
            "role" => "Initiator",
            "image" => "https://cdn.discordapp.com/attachments/900838594212659211/1161332453219238040/250.png?ex=6537ea21&is=65257521&hm=7f9932a08c2f0b6cc8161231434b092a23667ebd5bf88c37c825d882bca24f1b&",
        ]);
        Character::create([
            "quote" => "Here comes the party! / Fire in the hole!",
            "hero" => "Raze",
            "role" => "Duelist",
            "image" => "https://cdn.discordapp.com/attachments/900838594212659211/1161332568176734288/250.png?ex=6537ea3c&is=6525753c&hm=17f75aed410f0908879aef287a3f19f10f1ca7d8786e68ea2dd18da150d8ed6d&",
        ]);
        Character::create([
            "quote" => "They will cower! / The hunt begins!",
            "hero" => "Reyna",
            "role" => "Duelist",
            "image" => "https://cdn.discordapp.com/attachments/900838594212659211/1161332642503983314/250.png?ex=6537ea4e&is=6525754e&hm=73f99abb049fadc0bbb1bf14662df7e675e8d33226ced8860a5135723d916504&",    
        ]);
        Character::create([
            "quote" => "Come on, let`s go! / Jokes over, you`re dead!",
            "hero" => "Phoenix",
            "role" => "Duelist",
            "image" => "https://cdn.discordapp.com/attachments/900838594212659211/1161332728776622100/250.png?ex=6537ea63&is=65257563&hm=c34f1cb52583c05fa85e492006db5691e35aa59e187129a5e66016dd6dc340eb&",
        ]);
        Character::create([
            "quote" => "Where is everyone hiding? / I know EXACTLY where you are!",
            "hero" => "Cypher",
            "role" => "Sentinel",
            "image" => "https://cdn.discordapp.com/attachments/900838594212659211/1161332815732949133/250.png?ex=6537ea77&is=65257577&hm=822b40b33be06c8dccf5587b2a1f3d28128893ee1a985f9576db1302bac34387&",    
        ]);
        Character::create([
            "quote" => "Watch them run! / Scatter!",
            "hero" => "Omen",
            "role" => "Controller",
            "image" => "https://cdn.discordapp.com/attachments/900838594212659211/1161334051446198342/250.png?ex=6537eb9e&is=6525769e&hm=a7e8996ce407dd5e8e18f253872e136132889bb4444fc07c929210d0920f111f&",
        ]);
        Character::create([
            "quote" => "I am the hunter! / Nowhere to run!",
            "hero" => "Sova",
            "role" => "Initiator",
            "image" => "https://cdn.discordapp.com/attachments/900838594212659211/1161334141472743464/250.png?ex=6537ebb3&is=652576b3&hm=ee3310f24f05e543194ec3b00f9ca7992257c8882ee222b077e4927da6800558&",            
        ]);
        Character::create([
            "quote" => "Open up the sky! / Prepare for hellfire!",
            "hero" => "Brimstone",
            "role" => "Controller",
            "image" => "https://cdn.discordapp.com/attachments/900838594212659211/1161334251527077928/250.png?ex=6537ebce&is=652576ce&hm=67fd09bcf4bcde4bc7ac12c0384521de98393db4fb8240ffc65fcc26cf3336e6&",
        ]);
        Character::create([
            "quote" => "Your duty is not over! / You will not kill my allies!",
            "hero" => "Sage",
            "role" => "Sentinel",
            "image" => "https://cdn.discordapp.com/attachments/900838594212659211/1161334331659276429/250.png?ex=6537ebe1&is=652576e1&hm=71f83069273d8b769b32501bd484aa19d5fa8499f9b7682b9ceee59717314b39&",
        ]);
        Character::create([
            "quote" => "Initiated! / You should run!",
            "hero" => "Killjoy",
            "role" => "Sentinel",
            "image" => "https://cdn.discordapp.com/attachments/900838594212659211/1161335324920774727/250.png?ex=6537ecce&is=652577ce&hm=377ea45e28da87ae4fa9ac393ad3de93b1907b603bd1dacac0a381fbc66859ac&",
        ]);
        Character::create([
            "quote" => "Seek them out! / I`ve got your trail!",
            "hero" => "Skye",
            "role" => "Initiator",
            "image" => "https://cdn.discordapp.com/attachments/900838594212659211/1161335388535795762/250.png?ex=6537ecdd&is=652577dd&hm=9c48f9e585f97f8064edfd8da5b04672eb1be7b263fb3ed47a29755a7e65c0a8&",    
        ]);
        Character::create([
            "quote" => "I`ll handle this! / You`re next!",
            "hero" => "Yoru",
            "role" => "Duelist",
            "image" => "https://cdn.discordapp.com/attachments/900838594212659211/1161335466860232845/250.png?ex=6537ecef&is=652577ef&hm=e3ea42983da6e37d20f0f9e16e6b1762db0b94d213940dc4a01b6acbcdc9ab76&",
        ]);
        Character::create([
            "quote" => "World divided! / You`re divided!",
            "hero" => "Astra",
            "role" => "Controller",
            "image" => "https://cdn.discordapp.com/attachments/900838594212659211/1161335547587993620/250.png?ex=6537ed03&is=65257803&hm=c3053599b0c3da6ac0094b570215b29fbc25fee50273f48c0368788f44fbda6d&",
        ]);
        Character::create([
            "quote" => "No one walks away! / You. Are. Powerless!",
            "hero" => "KAY/O",
            "role" => "Initiator",
            "image" => "https://cdn.discordapp.com/attachments/900838594212659211/1161335648196772001/250.png?ex=6537ed1b&is=6525781b&hm=2589d31512a31bba0e3dfc6d24bc190d230de24d49297b31dc64cb364d2373b5&",
        ]);
        Character::create([
            "quote" => "They are so dead! / You want to play? Let`s play!",
            "hero" => "Chamber",
            "role" => "Sentinel",
            "image" => "https://cdn.discordapp.com/attachments/900838594212659211/1161335712038273164/250.png?ex=6537ed2a&is=6525782a&hm=8ed37f93268095a8462ea0254fa399ffce42169f7023a8baf8eda0543358c963&",
        ]);
        Character::create([
            "quote" => "Here we go! / Oy! I`m pissed!",
            "hero" => "Neon",
            "role" => "Duelist",
            "image" => "https://cdn.discordapp.com/attachments/900838594212659211/1161335776697667654/250.png?ex=6537ed39&is=65257839&hm=63941d9d1d9243f4e26a63edb2bc81b90ae9be6cba6ec233b308bbc838f53988&", 
        ]);
        Character::create([
            "quote" => "Face your fear! / Nightmare take them!",
            "hero" => "Fade",
            "role" => "Initiator",
            "image" => "https://cdn.discordapp.com/attachments/900838594212659211/1161335876899573803/250.png?ex=6537ed51&is=65257851&hm=58fc9c69092a29e7fef71ae58131ec05f7b826bb3f62c34463b8f2bfc9d64a56&",    
        ]);
        Character::create([
            "quote" => "Let`s turn the tide! / I suggest you move!",
            "hero" => "Harbor",
            "role" => "Controller",
            "image" => "https://cdn.discordapp.com/attachments/900838594212659211/1161335954926227566/250.png?ex=6537ed64&is=65257864&hm=6460f2dcb2735d4dd9b95e77fc70d63a71a98a8506133dcbe854c5704f9c4bac&",
        ]);
        Character::create([
            "quote" => "It`s all you, lil` homie! / Oye! Monster on the loose!",
            "hero" => "Gekko",
            "role" => "Initiator",
            "image" => "https://cdn.discordapp.com/attachments/900838594212659211/1161336035033231430/250.png?ex=6537ed77&is=65257877&hm=40c6c8c43f88e1e702feae93874405fb0a09b6c3a08e516d1e84bfc6387403cc&",
        ]);
        Character::create([
            "quote" => "Pull them to their grave! / My territory, my rules!",
            "hero" => "Deadlock",
            "role" => "Sentinel",
            "image" => "https://cdn.discordapp.com/attachments/900838594212659211/1161336113001152522/250.png?ex=6537ed89&is=65257889&hm=bf1b80e0a7e2e5999274450a5ea6425477e734e60a27727af9b3cb39a944d838&",
        ]);
    }
}
