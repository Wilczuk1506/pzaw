<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\StorePublicationRequest;

class UpdatePublicationRequest extends StorePublicationRequest 
{
    /**
     * Determine if the user is authorized to make this request.
    */
}
