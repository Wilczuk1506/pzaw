<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePublicationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title' => ['required', 'string', 'min:3', 'max:50'],
            'content' => ['required', 'string', 'min:10', 'max:500'],
            'author_id' => ['required', 'numeric', 'exists:users,id'],
        ];
    }

    public function messages(): array
    {
        return [
            '*.numeric' => 'Input must be a number',
            '*.string' => 'Input must be a string',
            '*.required' => 'Input must be required',
            '*.min' => 'Input too short',
            '*.max' => 'Input too long'
        ];
    }
}
