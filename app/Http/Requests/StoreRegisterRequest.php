<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'min:5', 'max:20', 'unique:users,name'],
            'email' => ['required', 'string', 'regex:/^([a-z0-9+-]+)(.[a-z0-9+-]+)*@([a-z0-9-]+.)+[a-z]{2,6}$/ix', 'unique:users,email'],
            'password' => ['required', 'string', 'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/', 'confirmed'],
        ];
    }

    public function messages(): array
    {
        return [
            '*.required' => 'Input is required',
            'name.min' => 'Name too short [minimum 5 characters]',
            'name.max' => 'Name too long [maximum 20 characters]',
            'name.unique' => 'Name already in use',
            'email.regex' => 'Invalid email',
            'email.unique' => 'Email already in use',
            'password.regex' => 'Password must include: Uppercase and lowercase letters, a digit, a special character',
            'password.confirmed' => 'Must confirm password',
        ];
    }
}
