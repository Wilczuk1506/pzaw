<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Publication;
use Carbon\Carbon;
use App\Http\Requests\StorePublicationRequest;
use App\Http\Requests\UpdatePublicationRequest;
use App\Models\User;

class PublicationController extends Controller
{
    public function index() {
        $publications = Publication::orderByDesc('created_at')->get();
        return view('publications/index', [
            'publications' => $publications,
        ]);
    }

    public function show(int $id) {
        
        $publication = Publication::where('id', $id)->firstOrFail();
        $time_ago = Carbon::create($publication->created_at)->diffForHumans(Carbon::now());
        
        return view('publications/show',[
            'publication' => $publication,
            'data' => $time_ago,
            'comments' => $publication->comments,
        ]); 
    }

    public function create() {
        return view('publications/form', [
            'users' => User::all(),
        ]);
    }

    public function store(StorePublicationRequest $request) {
        $data = $request->validated();
        
        $newPublication = new Publication($data);
        
        $newPublication->save();

        return redirect()->route('PublicationsAll')->with('success', 'Akcja pomyślnie wykonana');
    }

    public function edit(Publication $publication){
        return view('publications/form', [
            'publication' => $publication,
            'users' => User::all(),
        ]);
    }

    public function update(UpdatePublicationRequest $request, Publication $publication){
        $data = $request->validated();
        $publication->fill($data);
        $publication->save();
        return redirect()->route('PublicationsId', ['id' => $publication->id])->with('success', 'Akcja pomyślnie wykonana');
    }

    public function destroy(Publication $publication){
        $publication->comments()->delete();
        $publication->delete();
        return redirect()->route('PublicationsAll')->with('success', 'Publikacja została usunięta');
    }
}