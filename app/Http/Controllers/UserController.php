<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests\StoreRegisterRequest;

class UserController extends Controller
{
    public function create(){
        return view('users/register');
    }

    public function store(StoreRegisterRequest $request){
        $data = $request->validated();

        $newUser = new User($data);

        // dd($data, $newUser);

        $newUser->save();

        return redirect()->route('home');
    }
}
