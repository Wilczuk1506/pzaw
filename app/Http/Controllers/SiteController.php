<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Publication;
use App\Models\Character;
use App\Models\User;
use Carbon\Carbon;

class SiteController extends Controller
{
    public function home() {
        $newest_publication = Publication::orderByDesc('created_at')->First();
        $time_ago = Carbon::create($newest_publication->created_at)->diffForHumans(Carbon::now());
        return view('home', [
            'date' => now(),
            'publication' => $newest_publication,
            'data' => $time_ago,
        ]);
    }

    public function about_us() {
        return view('about_us');
    }

    public function quotes() {
        $quotes = Character::all();
        return view('quotes', [
            'quotes' => $quotes,
        ]);
    }

    public function user(int $id) {
        $user = User::where('id', $id)->firstOrFail();

        return view('user', [
            'user' => $user,
        ]);
    }
}
