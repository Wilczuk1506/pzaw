<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $author_id
 * @property int $publication_id
 * @property string $content
 * 
 * @property-read User $author
 * @property-read Publication $publication
 * 
 */

class Comment extends Model
{
    protected $fillable = [
        'author_id',
        'publication_id',
        'content'
    ];

    public function author() {
        return $this->belongsTo(User::class, 'author_id')->withTrashed();
    }
}
