<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;

/**
* @property string $title
* @property string $content
* @property string $author_id
*
* @property-read User $author
*/


class Publication extends Model
{
    protected $fillable = [
        'title',
        'content',
        'author_id'
    ];

    protected function excerpt(): Attribute {
        return Attribute::make(
            get: fn() => substr($this->content, 0 , 49)."..."
        );
    }

    public function author() {
        return $this->belongsTo(User::class, 'author_id')->withTrashed();
    }

    public function comments() {
        return $this->hasMany(Comment::class, 'publication_id');
    }
}
