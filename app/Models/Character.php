<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;

/**
* @property string $quote
* @property string $hero
* @property string $role
* @property string $image
*/

class Character extends Model
{
    protected $fillable = [
        'quote',
        'hero',
        'role',
        'image',
    ];
}
